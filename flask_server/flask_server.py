from flask import *
app = Flask(__name__)
import functions
import time
import os

#config:
#ip du server rabbitMQ
RabbitMQServerIP = "10.0.2.15"
#liste des comptes utilisateurs
users = [["1","1"]]
#dernier id job
LastJobID = 0

GIT_LOCATION = "gitlab.com/RT0704-louis.velu/"


@app.route("/")
def index():
	return render_template("index.html")

@app.route("/read")
def read():
	## TODO
	return render_template("read.html")

@app.route("/write")
def write():
	# TODO
        return render_template("write.html")

@app.route("/submit", methods=["POST"])
def submit():
	data = json.loads(request.form["data"])
	if [data["id"],data["password"]] in users:
		global LastJobID
		LastJobID = LastJobID+1
		jobID = str(LastJobID)
		os.system("mkdir -p /tmp/"+jobID)
		request.files["file"].save("/tmp/"+jobID+"/"+request.files["file"].filename)
		functions.git_clone(GIT_LOCATION+"file_in","/tmp/file_in")
		functions.git_add("/tmp/file_in/","/tmp/"+jobID)
		functions.git_push("/tmp/file_in")
		functions.git_rm("/tmp/file_in/")
		os.system("rm -rf /tmp/"+jobID)
		job_request={
			"user_id": data["id"],
			"id": jobID,
			"job": {
				"type": data["type"],
				"file_in": request.files["file"].filename,
				"file_out": data["filename"],
				"parametre":{
					"format": data["format"],
					"args": data["args"]
				}
			}
		}
		functions.publish(RabbitMQServerIP, "ToDo", json.dumps(job_request))
		return json.dumps({"job_id": jobID})
	else:
        	return json.dumps({"erreur": "bad login"})

@app.route("/get-job")
#permet au workers de recevoir leurs job
def get_job():
	#print(request.args.get("data"), flush=True)
	data = json.loads(request.args["data"])
	method_frame, header_frame, body = functions.consume(RabbitMQServerIP, "ToDo")
	if method_frame:
		job_request = json.loads(body)
		# on ne garde que les info pour le worker
		job = {"id":job_request["id"], "job": job_request["job"]}
		# on ajoute quelques donnés à la requête du client
		job_request["worker_id"] = data["worker_id"]
		job_request["start_time"] = time.time()
		#on place la requête dans Running
		functions.publish(RabbitMQServerIP, "Running", json.dumps(job_request))
		return json.dumps(job)
	else:
		return json.dumps({"erreur":"Nothing to read"})

@app.route("/get-result")
#permet au client de reçevoir leurs résultat
def get_result():
	data = json.loads(request.args["data"])
	#check permet de sortir si on ne trouve pas le resultat
	check = []
	method_frame, header_frame, body = functions.consume(RabbitMQServerIP, "Done")
	if not method_frame:
		return json.dumps({"erreur": "result not available"})
	job_result = json.loads(body)
	while job_result["id"] != data["id"] and not job_result["id"] in check:
		#si on n'a pas trouvé le job, on le remet dans la file
		check.append(job_result["id"])
		functions.publish(RabbitMQServerIP, "Done", json.dumps(job_result))
		method_frame, header_frame, body = functions.consume(RabbitMQServerIP, "Done")
		job_result = json.loads(body)
	if job_result["id"] in check:
		#le résultat n'a pas été trouvé dans la file Done
		return json.dumps({"erreur": "result not available"})

	if "erreur" in job_result:
		return json.dumps({"erreur": job_result["erreur"]})
	else:
		functions.git_clone(GIT_LOCATION+"file_out.git", "/tmp/file_out/")
		os.system("mkdir -p /tmp/"+job_result["id"])
		os.system("mv /tmp/file_out/"+job_result["id"]+"/"+job_result["job"]["file_out"]+" /tmp/"+job_result["id"])
		functions.git_rm("/tmp/file_out/")
		return send_file("/tmp/"+job_result["id"]+"/"+job_result["job"]["file_out"],as_attachment=True, attachment_filename=job_result["job"]["file_out"])
		#TODO rm file ??

@app.route("/put-result", methods=["POST"])
#permet au workers de transmetre leurs résultats
def put_result():
	data = json.loads(request.form["data"])
        #check permet de sortir au cas ou on ne retrouve pas le job dans la file
	check = []
	method_frame, header_frame, body = functions.consume(RabbitMQServerIP, "Running")
	if not method_frame:
		return json.dumps({"erreur":"Result drop"})
	job_request = json.loads(body)
	while job_request["id"] != data["job_id"] and not job_request["id"] in check:
		#si on n'a pas trouvé le job, on le remet dans la file
		check.append(job_request["id"])
		functions.publish(RabbitMQServerIP, "Running", json.dumps(job_request))
		method_frame, header_frame, body = functions.consume(RabbitMQServerIP, "Running")
		job_request = json.loads(body)
	if job_request["id"] in check:
		# la requpête n'a pas été retrouvé dans la file Running, le résultat est drop
		print("Result drop:"+str(data), flush=True)
		return json.dumps({"erreur":"Result drop"})
	if "erreur" in data:
		#si c'est une erreur
		job_request["erreur"] = data["erreur"]
		job_request["end_time"] = time.time()
	else:
		#si le job c'est bien fini
		job_request["filename"] = data["filename"]
		job_request["stats"] = data["stats"]
		job_request["end_time"] = time.time()

	functions.publish(RabbitMQServerIP, "Done", json.dumps(job_request))
	return json.dumps({"status": "ok"})

@app.route("/get-state")
def get_state():
	## TODO
        return render_template("get-state.html")

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
