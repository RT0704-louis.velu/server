import functions
import time
import pika

#creation du server RabbitMQ
functions.docker_run("rabbitmq", detach=True, ports={5672:5672} )

#creation des queue sur le server RabbitMQ
#on essaye le temps que le server n'est pas lancé
print("wait for RabbitMQ server ...")
while True:
	try:
		functions.create_queue("127.0.0.1","ToDo")
		functions.create_queue("127.0.0.1","Done")
		functions.create_queue("127.0.0.1","Running")
		break
	except pika.exceptions.IncompatibleProtocolError:
		continue
#build du server web
functions.docker_build("./flask_server", "flask_server:latest")

#on lance le server web
functions.docker_run("flask_server", detach=True, ports={5000:5000} )
